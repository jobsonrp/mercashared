package com.example.android.bluetoothchat.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.android.bluetoothchat.dominio.Produto;
import com.example.android.dao.BDHelper;

import java.util.ArrayList;
import java.util.List;

public class ProdutoDAO {

    private Context context;
    private BDHelper bdHelper;
    private static final String SELECT = "SELECT * FROM ";
    private static final String WHERE = " WHERE ";
    private static final String LIKE = " LIKE ? ";

    public ProdutoDAO(Context contexto) {
        this.context = contexto;
        bdHelper = new BDHelper(context);
    }

    public final Produto buscar(String nome){
        SQLiteDatabase db = bdHelper.getReadableDatabase();
        Produto produto = null;
        Cursor cursor = db.rawQuery(SELECT + bdHelper.TBL_PRODUTO +
                WHERE + bdHelper.COLUNA_NOME_PRODUTO + LIKE, new String[]{nome});
        if (cursor.moveToFirst()){
            produto = criarProduto(cursor);
        }
        cursor.close();
        db.close();
        return produto;
    }

    public final List<Produto> listar(){
        List<Produto> produtos = new ArrayList<>();
        SQLiteDatabase db = bdHelper.getReadableDatabase();
        Cursor cursor = db.query(BDHelper.TBL_PRODUTO, null, null, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            produtos.add(criarProduto(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return produtos;
    }

    public final void cadastrar(Produto produto){
        SQLiteDatabase db = bdHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(bdHelper.COLUNA_DESCRICAO_PRODUTO, produto.getDescricao());
        values.put(bdHelper.COLUNA_NOME_PRODUTO, produto.getNome());

        db.insert(bdHelper.TBL_PRODUTO, null, values);
        db.close();
    }

    public final void excluir(Produto produto){
        SQLiteDatabase db = bdHelper.getWritableDatabase();
        String where = bdHelper.COLUNA_ID_PRODUTO+ "=" + produto.getId();
        db.delete(bdHelper.TBL_PRODUTO, where, null);
        db.close();
    }

    private Produto criarProduto(Cursor cursor){
        Produto produto = new Produto();
        produto.setId(cursor.getInt(0));
        final int columnIndex1 = 1;
        final int columnIndex2 = 2;

        produto.setNome(cursor.getString(columnIndex1));
        produto.setDescricao(cursor.getString(columnIndex2));
        return produto;
    }

}
