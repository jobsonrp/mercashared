package com.example.android.bluetoothchat.gui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.android.bluetoothchat.BluetoothChatService;
import com.example.android.bluetoothchat.Constants;
import com.example.android.bluetoothchat.DeviceListActivity;
import com.example.android.bluetoothchat.R;
import com.example.android.bluetoothchat.dao.ProdutoDAO;
import com.example.android.bluetoothchat.dominio.Produto;
import com.example.android.common.logger.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This fragment controls Bluetooth to communicate with other devices.
 */
public class ProdutosFragment extends Fragment {

    private static final String TAG = "ProdutosFragment";
    private Activity context;

    public AlertDialog getAlerta() {
        return alerta;
    }

    public void setAlerta(AlertDialog alerta) {
        this.alerta = alerta;
    }

    private AlertDialog alerta;

    @Override
    public final Activity getContext() {
        return context;
    }

    public final void setContext(Activity contexto) {
        this.context = contexto;
    }


    public final  View onCreateView(LayoutInflater inflater, ViewGroup container,
                                    Bundle savedInstanceState) {
        setContext(getActivity());
        return inflater.inflate(R.layout.fragment_produtos, container, false);
    }

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;

    private Produto produtoRead = new Produto();
    private ProdutoDAO produtoDaoContext = new ProdutoDAO(context);

    private List<Produto> produtosList = new ArrayList<Produto>();
    //private List<Produto> retornoList = new ArrayList<Produto>();
    private List<String> myListProds = new ArrayList<String>();

    private ListView mBoxView;

    private ListView lista;

    private ProdutoListAdapter dataAdapter;

    public final  ProdutoListAdapter getDataAdapter() {
        return dataAdapter;
    }

    public final void setDataAdapter(ProdutoListAdapter dataAdapters) {
        this.dataAdapter = dataAdapters;
    }
    public final ListView getLista() {
        return lista;
    }

    public final void setLista(ListView listas) {
        this.lista = listas;
    }


    /**
     * Name of the connected device
     */
    private String mConnectedDeviceName = null;

    /**
     * Array adapter for the conversation thread
     */
    //private ArrayAdapter<String> mConversationArrayAdapter;
    private ArrayAdapter<String> mProdutosArrayAdapter;
    //private ArrayAdapter<String> mProdCarrinhoArrayAdapter;

    /**
     * String buffer for outgoing messages
     */
    private StringBuffer mOutStringBuffer;

    /**
     * Local Bluetooth adapter
     */
    private BluetoothAdapter mBluetoothAdapter = null;

    /**
     * Member object for the chat services
     */
    private BluetoothChatService mChatService = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            FragmentActivity activity = getActivity();
            Toast.makeText(activity, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            activity.finish();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        // If BT is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } else if (mChatService == null) {
            setupChat();
        }

        Button bt3=(Button)getContext().findViewById(R.id.addProduto);
        bt3.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                CadastroProdutoFragment fragment2 = new CadastroProdutoFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.sample_content_fragment, fragment2);
                fragmentTransaction.commit();
            }
        });

        Button bt4=(Button)getContext().findViewById(R.id.enviarProdutos);
        bt4.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                if (mChatService.getState() != BluetoothChatService.STATE_CONNECTED) {
                    Toast.makeText(getActivity(), R.string.not_connected, Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    ProdutoDAO consulta = new ProdutoDAO(getContext());
                    produtosList = consulta.listar();
                    // Get the message bytes and tell the BluetoothChatService to write
                    String strList = produtosList.get(0).getNome() + "," + produtosList.get(0).getDescricao();
                    for(int i=1; i<produtosList.size(); i++)
                        strList += ":" + produtosList.get(i).getNome() + "," + produtosList.get(i).getDescricao();

                    byte[] send = strList.getBytes();
                    mChatService.write(send);
                    // Reset out string buffer to zero and clear the edit text field
                    mOutStringBuffer.setLength(0);
                }
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mChatService != null) {
            mChatService.stop();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mChatService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mChatService.getState() == BluetoothChatService.STATE_NONE) {
                // Start the Bluetooth chat services
                mChatService.start();
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mBoxView = (ListView) view.findViewById(R.id.listProdutosFrag);
        //mCarrinhoView = (ListView) view.findViewById(R.id.listProdCarrinho);
    }

    /**
     * Set up the UI and background operations for chat.
     */
    private void setupChat() {
        Log.d(TAG, "setupChat()");

        // Initialize the array adapter for the conversation thread
        //mConversationArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.message);
        mProdutosArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.formato_lista_checkbox);
        //mProdCarrinhoArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.message);

        //mConversationView.setAdapter(mConversationArrayAdapter);
        mBoxView.setAdapter(mProdutosArrayAdapter);
        //mCarrinhoView.setAdapter(mProdCarrinhoArrayAdapter);

        // Initialize the compose field with a listener for the return key
        //mOutEditText.setOnEditorActionListener(mWriteListener);

        ProdutoDAO consulta = new ProdutoDAO(getContext());

        produtosList = consulta.listar();

        FragmentActivity activity2 = getActivity();
        setDataAdapter(new ProdutoListAdapter(activity2, produtosList));

        setLista((ListView) getView().findViewById(R.id.listProdutosFrag));
        getLista().setAdapter(getDataAdapter());
        getLista().setTextFilterEnabled(true);

        getLista().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View view, int position, long id) {
                // Rotina
            }
        });

        getLista().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> listView, View view, int position, long id) {
                Produto produto = (Produto) listView.getItemAtPosition(position);
                if (produto != null) {
                    //alertDeletarItem(produto, position);
                    showDialog(produto.getNome(),position);
                    /**ExcluirFragment fragmentEcluir = new ExcluirFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("Nome",produto.getNome());
                    bundle.putString("Desc",produto.getDescricao());
                    fragmentEcluir.setArguments(bundle);
                    chamarExcluirFragment(fragmentEcluir);*/
                }
                return true;
            }
        });

        // Initialize the BluetoothChatService to perform bluetooth connections
        mChatService = new BluetoothChatService(getActivity(), mHandler);

        // Initialize the buffer for outgoing messages
        mOutStringBuffer = new StringBuffer("");
    }

    private void showDialog(final String nomeProd, final int pos){

        final Dialog mydiag = new Dialog(getContext());

        mydiag.setContentView(R.layout.fragment_excluir);

        Button BtnNao = (Button) mydiag.findViewById(R.id.btnNao);

        BtnNao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Ação cancelada pelo usuário.", Toast.LENGTH_SHORT).show();
                mydiag.dismiss();
            }
        });

        Button BtnSim = (Button) mydiag.findViewById(R.id.btnSim);

        BtnSim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProdutoDAO produtoDao = new ProdutoDAO(context);
                Produto produtoExcluir = produtoDao.buscar(nomeProd);
                produtoDao.excluir(produtoExcluir);
                Toast.makeText(getContext(), "Produto " + produtoExcluir.getNome() + " deletado.", Toast.LENGTH_SHORT).show();
                getDataAdapter().remove(getDataAdapter().getItem(pos));
                getDataAdapter().notifyDataSetChanged();
                mydiag.dismiss();
            }
        });

        mydiag.show();
    }


    public void chamarExcluirFragment(Fragment fragment2){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.sample_content_fragment, fragment2);
        fragmentTransaction.commit();
    }

    private void alertDeletarItem(final Produto produto, final int position) {
        // excluir Produtos.
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(produto.getNome());
        builder.setMessage("Deseja deletar o produto?");
        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                ProdutoDAO excluirProd = new ProdutoDAO(getContext());
                excluirProd.excluir(produto);
                Toast.makeText(getContext(), "Produto " + produto.getNome() + " deletado.", Toast.LENGTH_SHORT).show();
                getDataAdapter().remove(getDataAdapter().getItem(position));
                getDataAdapter().notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Toast.makeText(getContext(), "Ação cancelada pelo usuário.", Toast.LENGTH_SHORT).show();
            }
        });
        setAlerta(builder.create());
        getAlerta().show();
    }

    private Produto criarProduto(String nome, String descricao){
        Produto produtoCadastro = new Produto();
        produtoCadastro.setDescricao(descricao);
        produtoCadastro.setNome(nome);

        return produtoCadastro;
    }

    /**
     * Makes this device discoverable for 300 seconds (5 minutes).
     */
    private void ensureDiscoverable() {
        if (mBluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }

    /**
     * Updates the status on the action bar.
     *
     * @param resId a string resource ID
     */
    private void setStatus(int resId) {
        FragmentActivity activity = getActivity();
        if (null == activity) {
            return;
        }
        final ActionBar actionBar = activity.getActionBar();
        if (null == actionBar) {
            return;
        }
        actionBar.setSubtitle(resId);
    }

    /**
     * Updates the status on the action bar.
     *
     * @param subTitle status
     */
    private void setStatus(CharSequence subTitle) {
        FragmentActivity activity = getActivity();
        if (null == activity) {
            return;
        }
        final ActionBar actionBar = activity.getActionBar();
        if (null == actionBar) {
            return;
        }
        actionBar.setSubtitle(subTitle);
    }

    /**
     * The Handler that gets information back from the BluetoothChatService
     */
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            FragmentActivity activity = getActivity();
            switch (msg.what) {
                case Constants.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothChatService.STATE_CONNECTED:
                            setStatus(getString(R.string.title_connected_to, mConnectedDeviceName));
                            //mConversationArrayAdapter.clear();
                            break;
                        case BluetoothChatService.STATE_CONNECTING:
                            setStatus(R.string.title_connecting);
                            break;
                        case BluetoothChatService.STATE_LISTEN:
                        case BluetoothChatService.STATE_NONE:
                            setStatus(R.string.title_not_connected);
                            break;
                    }
                    break;
                case Constants.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    List<Produto> writeList = new ArrayList<>();

                    try {
                        int testeString = 9999;
                        testeString = Integer.parseInt(writeMessage);
                    } catch(NumberFormatException nfe) {
                        //mProdCarrinhoArrayAdapter.add(writeMessage);
                    }
                    break;
                case Constants.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);

                    List<String> myList = new ArrayList<String>(Arrays.asList(readMessage.split(":")));
                    //Log.d(TAG, "String List PPPP: " + myList.toString());

                    //List<Produto> retornoList = new ArrayList<>();
                    for(int j=0; j<myList.size(); j++) {
                        myListProds = new ArrayList<String>(Arrays.asList(myList.get(j).split(",")));
                        Produto produtoRead = criarProduto(myListProds.get(0), myListProds.get(1));

                        ProdutoDAO produtoDao = new ProdutoDAO(context);
                        Produto resultBusca = produtoDao.buscar(myListProds.get(0));
                        if (resultBusca == null) {
                            produtoDao.cadastrar(produtoRead);
                            //Log.d(TAG, "String Result 7777777777: " + produtoRead);
                        }
                        setupChat();
                        //getDataAdapter().notifyDataSetChanged();
                        //Log.d(TAG, "String QQQQQQQ: " + produtoRead);
                        //retornoList.add(produtoRead);
                        //Log.d(TAG, "aaaaaaaaaaaaaaaaaaaaa: " + produtoRead.toString());
                    }
                    break;
                case Constants.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);
                    if (null != activity) {
                        Toast.makeText(activity, "Connected to "
                                + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    }
                    break;
                case Constants.MESSAGE_TOAST:
                    if (null != activity) {
                        Toast.makeText(activity, msg.getData().getString(Constants.TOAST),
                                Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    };

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, false);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    setupChat();
                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "BT not enabled");
                    Toast.makeText(getActivity(), R.string.bt_not_enabled_leaving,
                            Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }
        }
    }

    /**
     * Establish connection with other device
     *
     * @param data   An {@link Intent} with {@link DeviceListActivity#EXTRA_DEVICE_ADDRESS} extra.
     * @param secure Socket Security type - Secure (true) , Insecure (false)
     */
    private void connectDevice(Intent data, boolean secure) {
        // Get the device MAC address
        String address = data.getExtras()
                .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        // Get the BluetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
        mChatService.connect(device, secure);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.bluetooth_chat, menu);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.secure_connect_scan: {
                // Launch the DeviceListActivity to see devices and do scan
                Intent serverIntent = new Intent(getActivity(), DeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
                return true;
            }
            case R.id.insecure_connect_scan: {
                // Launch the DeviceListActivity to see devices and do scan
                Intent serverIntent = new Intent(getActivity(), DeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_INSECURE);
                return true;
            }
            case R.id.discoverable: {
                // Ensure this device is discoverable by others
                ensureDiscoverable();
                return true;
            }
        }
        return false;
    }

}
