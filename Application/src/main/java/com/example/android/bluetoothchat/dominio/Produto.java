package com.example.android.bluetoothchat.dominio;

public class Produto {
    private int id;
    private String descricao;
    private String nome;

    public String toString() { return nome; }

    public final String getNome() {
        return nome;
    }

    public final void setNome(String nomes) {
        this.nome = nomes;
    }

    public final String getDescricao() {
        return descricao;
    }

    public final void setDescricao(String descricaos) {
        this.descricao = descricaos;
    }

    public final int getId() {
        return id;
    }

    public final void setId(int ids) {
        this.id = ids;
    }

}
