package com.example.android.bluetoothchat.gui;

import android.app.Activity;

import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;

import com.example.android.bluetoothchat.R;

/**
 * This fragment controls Bluetooth to communicate with other devices.
 */
public class TelaInicialFragment extends Fragment {

    private static final String TAG = "CadastroProdutoFragment";

    private Activity context;

    @Override
    public final Activity getContext() {
        return context;
    }

    public final void setContext(Activity contexto) {
        this.context = contexto;
    }

    public final  View onCreateView(LayoutInflater inflater, ViewGroup container,
                                    Bundle savedInstanceState) {
        setContext(getActivity());
        return inflater.inflate(R.layout.fragment_tela_inicial, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onStart() {
        super.onStart();

        Button bt3=(Button)getContext().findViewById(R.id.btnProdutos);
        bt3.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {

                ProdutosFragment fragment2 = new ProdutosFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager(); //getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.sample_content_fragment, fragment2);
                fragmentTransaction.commit();

            }
        });

        Button bt4=(Button)getContext().findViewById(R.id.btnListasCompras);
        bt4.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {

                ListasComprasFragment fragment3 = new ListasComprasFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.sample_content_fragment, fragment3);
                fragmentTransaction.commit();

            }
        });

    }
}

