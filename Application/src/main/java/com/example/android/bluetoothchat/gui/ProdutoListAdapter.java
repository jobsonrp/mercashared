package com.example.android.bluetoothchat.gui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.android.bluetoothchat.R;
import com.example.android.bluetoothchat.dominio.Produto;

import java.util.List;

public class ProdutoListAdapter extends ArrayAdapter<Produto> {

        private Context context;
        private List<Produto> produtos;

        public ProdutoListAdapter(Context contexto, List<Produto> produto) {
            super(contexto, 0, produto);
            this.context = contexto;
            this.produtos = produto;
        }

        @Override
        public  final View getView(int position, View converterTela, ViewGroup parent) {
            Produto produtoPosicao = this.produtos.get(position);

            converterTela = LayoutInflater.from(this.context).inflate(R.layout.formato_lista_checkbox, null);

            TextView nome = (TextView) converterTela.findViewById(R.id.produtoText);
            nome.setText(produtoPosicao.getNome().toString());

            TextView descricao = (TextView) converterTela.findViewById(R.id.descricaoText);
            descricao.setText(produtoPosicao.getDescricao().toString());

            return converterTela;
        }
}
