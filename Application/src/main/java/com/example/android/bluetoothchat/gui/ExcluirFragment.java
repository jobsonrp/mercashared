package com.example.android.bluetoothchat.gui;

import android.app.Activity;

import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.Toast;

import com.example.android.bluetoothchat.R;
import com.example.android.bluetoothchat.dao.ProdutoDAO;
import com.example.android.bluetoothchat.dominio.Produto;

/**
 * This fragment controls Bluetooth to communicate with other devices.
 */
public class ExcluirFragment extends Fragment {

    private static final String TAG = "CadastroProdutoFragment";

    private Activity context;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private String nome,desc;

    @Override
    public final Activity getContext() {
        return context;
    }

    public final void setContext(Activity contexto) {
        this.context = contexto;
    }

    public final  View onCreateView(LayoutInflater inflater, ViewGroup container,
                                    Bundle savedInstanceState) {
        setContext(getActivity());
        Bundle bundle = this.getArguments();
        nome = bundle.getString("Nome");
        desc = bundle.getString("Desc");
        Toast.makeText(getContext(), bundle.getString("Nome"), Toast.LENGTH_SHORT).show();
        return inflater.inflate(R.layout.fragment_excluir, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onStart() {
        super.onStart();

        Button bt3=(Button)getContext().findViewById(R.id.btnNao);
        bt3.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                chamarProdutosFragment();
                Toast.makeText(getContext(), "Ação cancelada pelo usuário.", Toast.LENGTH_SHORT).show();
            }
        });

        Button bt4=(Button)getContext().findViewById(R.id.btnSim);
        bt4.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                ProdutoDAO produtoDao = new ProdutoDAO(context);
                Produto produtoExcluir = produtoDao.buscar(nome);
                produtoDao.excluir(produtoExcluir);
                Toast.makeText(getContext(), "Produto " + produtoExcluir.getNome() + " deletado.", Toast.LENGTH_SHORT).show();
                chamarProdutosFragment();
            }
        });

    }

    public void chamarProdutosFragment(){
        ProdutosFragment fragment2 = new ProdutosFragment();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.sample_content_fragment, fragment2);
        fragmentTransaction.commit();
    }

}

