package com.example.android.bluetoothchat.gui;

import android.app.Activity;

import android.content.Context;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.bluetoothchat.R;
import com.example.android.bluetoothchat.dao.ProdutoDAO;
import com.example.android.bluetoothchat.dominio.Produto;

/**
 * This fragment controls Bluetooth to communicate with other devices.
 */
public class CadastroProdutoFragment extends Fragment {

    private static final String TAG = "CadastroProdutoFragment";

    private Activity context;

    @Override
    public final Activity getContext() {
        return context;
    }

    public final void setContext(Activity contexto) {
        this.context = contexto;
    }

    public final  View onCreateView(LayoutInflater inflater, ViewGroup container,
                                    Bundle savedInstanceState) {
        setContext(getActivity());
        return inflater.inflate(R.layout.fragment_cadastro_produto, container, false);
    }

    /**@Override
    public final Activity getContext() {
        return context;
    }

    public final void setContext(Activity contexto) {
        this.context = contexto;
    }

    public final  View onCreateView(View view, LayoutInflater inflater, ViewGroup container,
                                    Bundle savedInstanceState) {
        setContext(getActivity());

        setnome = (EditText) getContext().findViewById(R.id.edtNomeProduto);
        setdescricao = (EditText) getContext().findViewById(R.id.edtDescricaoProduto);
        btnSalvar =(Button) getContext().findViewById(R.id.btnSalvarProduto);
        return inflater.inflate(R.layout.fragment_cadastro_produto, container, false);
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onStart() {
        super.onStart();

        final EditText setnome = (EditText) getContext().findViewById(R.id.edtNomeProduto);
        final EditText setdescricao = (EditText) getContext().findViewById(R.id.edtDescricaoProduto);

        Button btnSalvar =(Button) getContext().findViewById(R.id.btnSalvarProduto);
        btnSalvar.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                String nome = setnome.getText().toString().trim();
                String descricao = setdescricao.getText().toString().trim();

                Produto produto = criarProduto(nome, descricao);
                //Toast.makeText(getActivity(), " Nome: " + produto.getNome(), Toast.LENGTH_SHORT).show();

                ProdutoDAO produtoDao = new ProdutoDAO(context);
                produtoDao.cadastrar(produto);

                ProdutosFragment fragment2 = new ProdutosFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.sample_content_fragment, fragment2);
                fragmentTransaction.commit();
            }
        });

    }

    private Produto criarProduto(String nome, String descricao){
        Produto produtoCadastro = new Produto();
        produtoCadastro.setDescricao(descricao);
        produtoCadastro.setNome(nome);

        return produtoCadastro;
    }
}