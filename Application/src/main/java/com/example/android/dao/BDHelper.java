package com.example.android.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BDHelper extends SQLiteOpenHelper {

    public static final String NOME_BD = "mercapp.sqlite";
    public static final int VERSAO_BD = 1;

    //Tabela Produto
    public static final String TBL_PRODUTO = "ProdutoDAO";
    public static final String COLUNA_ID_PRODUTO = "_id";
    public static final String COLUNA_DESCRICAO_PRODUTO = "descricao";
    public static final String COLUNA_NOME_PRODUTO = "nomeProduto";

    public BDHelper(Context context) {
        super(context, NOME_BD, null, VERSAO_BD);
    }

    @Override
    public final void onCreate(SQLiteDatabase db) {
        db.execSQL(SQLScript.getTabelaProduto());
        CarregamentoDadosBD.carregarProdutosBD(db);
    }

    @Override
    public final void onUpgrade(SQLiteDatabase db, int versaoAntiga, int novaVersao) {
        final String testeTabelaExiste = "DROP TABLE IF EXISTIS ";
        db.execSQL(testeTabelaExiste + TBL_PRODUTO);
        onCreate(db);
    }
}
