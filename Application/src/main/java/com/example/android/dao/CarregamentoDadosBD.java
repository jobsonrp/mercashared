package com.example.android.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;

public class CarregamentoDadosBD extends AppCompatActivity {

    private final Context context = getApplicationContext();

    private static String inserirProdutosBD = "INSERT INTO " + BDHelper.TBL_PRODUTO + " (" +
            BDHelper.COLUNA_NOME_PRODUTO +" , " +
            BDHelper.COLUNA_DESCRICAO_PRODUTO +
            ") VALUES  ";

    public static void carregarProdutosBD(SQLiteDatabase db){
        carregarProduto(db, "arroz", "integral");
        carregarProduto(db, "coca-cola", "lata");
    }

    private static void carregarProduto(SQLiteDatabase db, String nome, String descricao)
    {
        db.execSQL(inserirProdutosBD +"('"+nome+"', '"+descricao+"');");
    }

}
