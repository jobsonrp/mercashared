package com.example.android.dao;

public final class SQLScript {

    private SQLScript() {
    }

    static String getTabelaProduto() {

        StringBuilder produtoBuilder = new StringBuilder();
        produtoBuilder.append("CREATE TABLE  ProdutoDAO  (  ");
        produtoBuilder.append("_id integer primary key autoincrement,   ");
        produtoBuilder.append("nomeProduto  text not null,  ");
        produtoBuilder.append("descricao  text not null );  ");

        return produtoBuilder.toString();
    }
}

